package mohamedfadel91.com.clueappdirector;

import android.Manifest;
import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,View.OnClickListener, Animator.AnimatorListener {

    private static final int MY_PERMISSIONS_REQUEST_GPS_LOCATION = 0x01;
    private static final String TAG = MainActivity.class.getSimpleName();

    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private String mDestinationLat;
    private String mDestinationLng;
    private Button mGoButton;
    private ImageView mLogoView;
    private RelativeLayout mParent;
    private boolean mIsGooglePlayServicesAvailable;
    private MapFragment mMapFragment;
    private ApiInterface mApiService;

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // for GPU Performance..
        getWindow().setBackgroundDrawable(null);


        mParent = (RelativeLayout) findViewById(R.id.main_container);
        mLogoView = (ImageView) findViewById(R.id.main_logo);
        mGoButton = (Button) findViewById(R.id.main_goBtn);

        mGoButton.setBackgroundColor(getResources().getColor(R.color.buttonInitialColor));

        mGoButton.setOnClickListener(this);

        mLogoView.animate().translationYBy(-100).setDuration(1000);
        mLogoView.animate().alphaBy(0.75f).setListener(this);

        mIsGooglePlayServicesAvailable = isGooglePlayServicesAvailable(this);
        setUpGoogleAPIClient();

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) mGoogleApiClient.connect();
    }

    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient != null) mGoogleApiClient.disconnect();

    }


    @SuppressWarnings("deprecation")
    @Override
    public void onMapReady(final GoogleMap googleMap) {
        if (mLastLocation == null){
            destroyMap2();
            return;
        }


        LatLng here = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        googleMap.setMyLocationEnabled(true);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(here, 10));

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://maps.googleapis.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mApiService = retrofit.create(ApiInterface.class);


        Call<Response2> locationCall = mApiService.getPlaceDetails(getString(R.string.clueapps_location_id), getString(R.string.API_KEY));
        locationCall.enqueue(new Callback<Response2>() {
            @Override
            public void onResponse(Call<Response2> call, Response<Response2> response) {

                // for GPU Performance..
                mParent.setBackgroundDrawable(null);

                String status = response.body().getStatus();
                if (status.equals("OK")) {
                    mDestinationLat = String.valueOf(response.body().getResult().getGeometry().getLocation().getLat());
                    mDestinationLng = String.valueOf(response.body().getResult().getGeometry().getLocation().getLng());

                    LatLng clueApp = new LatLng(Double.parseDouble(mDestinationLat), Double.parseDouble(mDestinationLng));
                    googleMap.addMarker(new MarkerOptions()
                            .title("ClueAPPs")
                            .position(clueApp));

                    Call<Response_> routeCall = mApiService.getRouteDetails(mDestinationLat + "," + mDestinationLng, mLastLocation.getLatitude() + "," + mLastLocation.getLongitude(),
                            "metric", "driving");

                    routeCall.enqueue(new Callback<Response_>() {
                        @Override
                        public void onResponse(Call<Response_> call, Response<Response_> response) {
                            List<LatLng> sectors = decodePoly(response.body().getRoutes().get(0).getOverviewPolyline().getPoints());
                            googleMap.addPolyline(new PolylineOptions()
                                    .addAll(sectors)
                                    .width(12)
                                    //Google maps blue color
                                    .color(Color.parseColor("#05b1fb"))
                                    .geodesic(true)
                            );


                        }

                        @Override
                        public void onFailure(Call<Response_> call, Throwable t) {
                            destroyMap();
                        }

                    });


                } else {
                    destroyMap();
                }

            }

            @Override
            public void onFailure(Call<Response2> call, Throwable t) {
                destroyMap();
            }
        });


    }

    @SuppressWarnings("deprecation")
    private void destroyMap() {
        getFragmentManager().beginTransaction().remove(mMapFragment).commit();
        mParent.setBackgroundColor(getResources().getColor(android.R.color.background_light));
        mGoButton.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_dark));
        mGoButton.setEnabled(true);
        Toast.makeText(MainActivity.this, "Please check your internet availability, then try again later", Toast.LENGTH_SHORT).show();

    }

    @SuppressWarnings("deprecation")
    private void destroyMap2() {
        getFragmentManager().beginTransaction().remove(mMapFragment).commit();
        mParent.setBackgroundColor(getResources().getColor(android.R.color.background_light));
        mGoButton.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_dark));
        mGoButton.setEnabled(true);
        Toast.makeText(MainActivity.this, "Cannot get your location now", Toast.LENGTH_LONG).show();

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSIONS_REQUEST_GPS_LOCATION);
            Log.i(TAG,"hero");
            return;
        }
        updateMyLocation();
    }

    private void updateMyLocation() {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null)
            Log.i(TAG, "Get Current Location Successfully!!");
        else
            Log.e(TAG, "Failed to get Current Location");

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }

    @Override
    public void onClick(View v) {

        if (!mIsGooglePlayServicesAvailable) return;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSIONS_REQUEST_GPS_LOCATION);
            return;
        }
        updateMyLocation();

        mGoButton.setBackgroundColor(Color.GRAY);
        mGoButton.setEnabled(false);

        mMapFragment = MapFragment.newInstance();
        getFragmentManager().beginTransaction().replace(R.id.main_container, mMapFragment).commit();
        mMapFragment.getMapAsync(this);
    }

    @Override
    public void onAnimationStart(Animator animation) {

    }

    @Override
    public void onAnimationEnd(Animator animation) {
        mGoButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void onAnimationCancel(Animator animation) {

    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }

    private void setUpGoogleAPIClient() {
        if (mGoogleApiClient == null && mIsGooglePlayServicesAvailable) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    private boolean isGooglePlayServicesAvailable(Context context) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = googleApiAvailability.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            googleApiAvailability.getErrorDialog((Activity) context, resultCode, 2404).show();
            return false;
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mLogoView.clearAnimation();
        if (mParent != null) mParent.removeAllViews();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_GPS_LOCATION:
                updateMyLocation();
                break;

            default:
                break;
        }
    }

}
