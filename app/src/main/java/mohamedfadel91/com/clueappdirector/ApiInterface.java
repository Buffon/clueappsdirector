package mohamedfadel91.com.clueappdirector;

        import retrofit2.Call;
        import retrofit2.http.GET;
        import retrofit2.http.Query;

/**
 * Created by BUFFON on 9/23/2016.
 */
public interface ApiInterface {
    @GET("maps/api/directions/json")
    Call<Response_> getRouteDetails(@Query("origin") String origin, @Query("destination") String destination,
                                @Query("units") String units, @Query("mode") String mode);

    @GET("maps/api/place/details/json")
    Call<Response2> getPlaceDetails(@Query("placeid") String id, @Query("key") String API_Key);


}
